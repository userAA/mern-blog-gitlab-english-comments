import React from 'react';
import {Routes, Route} from "react-router-dom";
import {useDispatch} from 'react-redux';
import Container from "@mui/material/Container";

import { Header } from "./components/Header/index.jsx";
import { Home } from "./pages/Home.jsx";
import { FullArticle } from "./pages/FullArticle.jsx";
import { Registration } from "./pages/Registration/index.jsx";
import { AddArticle } from "./pages/AddArticle/index.jsx";
import { Login } from "./pages/Login/index.jsx";
import { fetchAuthMe} from "./redux/slices/auth.js";

function App() {
  const dispatch = useDispatch();

  React.useEffect(() => {
    //making up the request for receiving data about authorized user, if such user exists
    dispatch(fetchAuthMe())
  }, [dispatch]);

  return (
    <>
      <Header />
      <Container maxWidth="lg">
        <Routes>
        {/*Refference on central page */}
        < Route path="/"               element={<Home />} />
        {/*Refference on page of full information about article */}
        < Route path="/articles/:id"      element={<FullArticle />} />
        {/*Refference on page of editing page */}
        < Route path="/articles/:id/edit" element={<AddArticle />} />
        {/*Refference on page of adding page */}
        < Route path="/add-article"       element={<AddArticle />} />
        {/*Refference on page of registered user authorization */}   
        < Route path="/login"          element={<Login />} />
        {/*Refference on page of user registration*/} 
        < Route path="/register"       element={<Registration />} />
        </Routes>
      </Container>
    </>
  );
}

export default App;
