import React from "react";
import styles from "./UserInfo.module.scss";

//Information about user, which created article
export const UserInfo = ({ 
  //full name of user
  fullName, 
  //the time of creating article by user 
  additionalText 
}) => {

  return (
    <div className={styles.root}>
      {/*Showing user avatar */}
      <img className={styles.avatar} src={"https://mui.com/static/images/avatar/5.jpg"} alt={fullName} />
      <div className={styles.userDetails}>
        {/*Showing user fullname */}
        <span className={styles.userName}>{fullName}</span>
        {/*Outputing the time of creating article by user */}
        <span className={styles.additional}>{additionalText}</span>
      </div>
    </div>
  );
};
