import React from "react";
import { useDispatch } from "react-redux";
import {Link} from "react-router-dom"
import clsx from "clsx";
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Clear';

import EyeIcon from "@mui/icons-material/RemoveRedEyeOutlined";

import styles from "./Article.module.scss";
import { UserInfo } from "../UserInfo";
import {ArticleSkeleton} from './Skeleton';
import { fetchRemoveArticle } from "../../redux/slices/articles";

export const Article = ({
  //ID article
  id,
  //name title 
  title,
  //Time of creating article
  createdAt,
  //Image article
  imageUrl,
  //Information about user, which created article
  user,
  //Views count of article
  viewsCount,
  //Views count of article
  tags,
  //Flag view of full information about article
  isFullArticle,
  //Flag of process loading of article
  isLoading,
  //Flag of process loading of article
  isEditable
}) => {

  const dispatch = useDispatch();
  //Unloading the template article ArticleSkeleton, if flag of loading article isLoading equals true
  if (isLoading) {
    return <ArticleSkeleton/>
  }

  //Removing article event
  const onClickRemove = () => {
    if (window.confirm('Do you really want to delete the article?'))
    {
      //Activating the request for removing article in case confirming removing
      dispatch(fetchRemoveArticle(id));
    }
  }

  return (
    <div className={clsx(styles.root, { [styles.rootFull]: isFullArticle })}>
      {/*Flag editing of article isEditable == true, if article is created by an authorized user */}
      {isEditable && (
        <div className={styles.editButtons}>
           {/*The button of editting article */}
          <Link to={`/articles/${id}/edit`}>
            <IconButton color="primary">
              <EditIcon/>
            </IconButton>
          </Link>
          {/*The button of removing article */}
          <IconButton onClick={onClickRemove} color = "secondary">
            <DeleteIcon/>
          </IconButton>
        </div>
      )}
      {/*Showing of image article*/}
      {imageUrl && ( 
        <img
          className={clsx(styles.image, { [styles.imageFull]: isFullArticle })}
          src={imageUrl}
          alt={title}
        />
      )}
      <div className={styles.wrapper}>
        {/*Outputing information about user, which created article and creating time of paper by user*/}
        <UserInfo {...user} additionalText={createdAt} />
        <div className={styles.indention}>
          {/*Outputing information about user, which created article*/}
          <h2 className={clsx(styles.title, { [styles.titleFull]: isFullArticle })}>
             {/*Writing the name article, or aside from this showing refference on demonstration full information about article*/}
            {isFullArticle ? title : <Link to={`/articles/${id}`}>{title}</Link>}
          </h2>
          {/*Showing tags article*/}
          <ul className={styles.tags}>
            {tags.map((name) => (
              <li key={name}>
                <Link to={`/tag/${name}`}>#{name}</Link>
              </li>
            ))}
          </ul>
          {/*View counter of article */}
          <ul className={styles.articleDetails}>
            <li>
              <EyeIcon />
              <span>{viewsCount}</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};