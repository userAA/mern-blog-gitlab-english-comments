import React from "react";
import {Link} from "react-router-dom"
import Button from "@mui/material/Button";

import styles from "./Header.module.scss";
import { Container } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { logout, selectIsAuth } from "../../redux/slices/auth";

//main page of project
export const Header = () => {
  const dispatch = useDispatch();
  //finding the flag of user authorization
  const isAuth = useSelector(selectIsAuth);

  const onClickLogout = () => {
    if (window.confirm('Do you really want to get out?'))
    {
      //making up request for removing data of store from user authorization
      dispatch(logout());
      //removing token of user authorization
      window.localStorage.removeItem('token');
    }
  }

  return (
    <div className={styles.root}>
      <Container maxWidth="lg">
        <div className={styles.inner}>
          {/*Switching link on main page*/}
          <Link className={styles.logo} to="/">
            <div>ROMANETS BLOG</div>
          </Link>
          <div className={styles.buttons}>
            {isAuth ? (
              <>
                {/*Actions, which are appropriate at existing of user authorization*/}
                {/*Link to writing an article by an authorized user*/}
                <Link to="/add-article">
                  {/*The button for writing an article by an authorized user*/}
                  <Button variant="contained">Write article</Button>
                </Link>
                {/*Button of clearing data of user authorization and removing of his token*/}
                <Button onClick={onClickLogout} variant="contained" color="error">
                  Escape
                </Button>
              </>
            ) : (
              <>
                {/*Actions, which are appropriate if authorized user does not exist*/}
                {/*Link on user authorization */}
                <Link to ="/login">
                  {/*Button user authorization*/}
                  <Button variant="outlined">Enter</Button>
                </Link>
                {/*Link on user registration */}
                <Link to ="/register">
                  {/*Button of user registration*/}
                  <Button variant="contained">Create account</Button>
                </Link>
              </>
            )}

          </div>
        </div>
      </Container>
    </div>
  );
};
