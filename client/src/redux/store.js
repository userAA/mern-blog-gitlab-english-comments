import {configureStore} from "@reduxjs/toolkit";
import { articlesReducer } from "./slices/articles";
import { authReducer } from "./slices/auth";

//creating a repository
const store = configureStore({
    reducer: {
        //collecting results from requests on actions with articles 
        articles: articlesReducer,
        //collecting results with requests on actions with user data 
        auth: authReducer
    }
})

export default store;