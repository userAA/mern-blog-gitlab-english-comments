import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axios from '../../axios';

//user authorization request
export const fetchAuth = createAsyncThunk('auth/fetchAuth', async (params) => {
    const { data } = await axios.post('/auth/login', params);
    return data;
})

//user registration request
export const fetchRegister = createAsyncThunk('auth/fetchRegister', async (params) => {
    const { data } = await axios.post('/auth/register', params);
    return data;
})

//request to receiving data on an authorized user, if such a user exists
export const fetchAuthMe = createAsyncThunk('auth/fetchAuthMe', async () => {
    const { data } = await axios.get('/auth/me');
    return data;
})

//initial state
const initialState = {
    data: null,
    status: 'loading'
};

const authSlice = createSlice ({
    name: 'auth',
    initialState,
    reducers: {
        //clearing data of authorized user 
        logout : (state) => {
            state.data = null;
        }
    },
    extraReducers: {
        //sending received data of authorized user to store 
        [fetchAuth.pending] : (state) => {
            state.status = 'loading';
            state.data = null;
        },
        [fetchAuth.fulfilled] : (state, action) => {
            state.status = 'loaded';
            state.data = action.payload;
        },
        [fetchAuth.rejected] : (state) => {
            state.status = 'error';
            state.data = null;
        },
        //sending received data of authorized user to store if such user exists
        [fetchAuthMe.pending] : (state) => {
            state.status = 'loading';
            state.data = null;
        },
        [fetchAuthMe.fulfilled] : (state, action) => {
            state.status = 'loaded';
            state.data = action.payload;
        },
        [fetchAuthMe.rejected] : (state) => {
            state.status = 'error';
            state.data = null;
        },
        //sending received data of registered user to store 
        [fetchRegister.pending] : (state) => {
            state.status = 'loading';
            state.data = null;
        },
        [fetchRegister.fulfilled] : (state, action) => {
            state.status = 'loaded';
            state.data = action.payload;
        },
        [fetchRegister.rejected] : (state) => {
            state.status = 'error';
            state.data = null;
        }             
    }
})

//exporting the flag of authorization user
export const selectIsAuth = state => Boolean(state.auth.data);
//this reducer is exported to the store
export const authReducer = authSlice.reducer;
//exporting the function of clearing data about authorized user
export const { logout } = authSlice.actions;