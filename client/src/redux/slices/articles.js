import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axios from '../../axios.js';

//request for receiving articles
export const fetchArticles = createAsyncThunk('/articles/fetchArticles', async () => {
    const {data} = await axios.get('/articles');
    return data;
})

//request for receiving tags
export const fetchTags = createAsyncThunk('/articles/fetchTags', async () => {
    const {data} = await axios.get('/tags');
    return data;
})

//request for removing of article according to specified ID id
export const fetchRemoveArticle = createAsyncThunk('/articles/fetchRemoveArticle', async (id) => {
    await axios.delete(`/articles/${id}`);
})

//initial state
const initialState = {
    articles: {
        items: [],
        status: 'loading'
    },
    tags: {
        items: [],
        status: 'loading'
    }
};

const articlesSlice = createSlice({
    name: 'articles',
    initialState,
    reducer: {},
    extraReducers: {
        //Receiving of articles
        [fetchArticles.pending] : (state) => {
            state.articles.items = [];
            state.articles.status = 'loading';
        },
        [fetchArticles.fulfilled] : (state, action) => {
            state.articles.items = action.payload;
            state.articles.status = 'loaded';
        },
        [fetchArticles.rejected] : (state) => {
            state.articles.items = [];
            state.articles.status = 'error';
        },
        //Receiving of tags
        [fetchTags.pending] : (state) => {
            state.tags.items = [];
            state.tags.status = 'loading';
        },
        [fetchTags.fulfilled] : (state, action) => {
            state.tags.items = action.payload;
            state.tags.status = 'loaded';
        },
        [fetchTags.rejected] : (state) => {
            state.tags.items = [];
            state.tags.status = 'error';
        },
        //Deleting of article
        [fetchRemoveArticle.pending] : (state, action) => {
            state.articles.items = state.articles.items.filter(obj => obj._id !== action.meta.arg);
        }
    }
})

//this reducer exporting in store
export const articlesReducer = articlesSlice.reducer;