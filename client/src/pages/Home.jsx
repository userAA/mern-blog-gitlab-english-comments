import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Grid from '@mui/material/Grid';

import { Article } from '../components/Article';
import { TagsBlock } from '../components/TagsBlock';
import {
    //finding request function on receiving all articles
    fetchArticles, 
    //finding request function on receiving tags on last five articles
    fetchTags    
} from '../redux/slices/articles';

//main page of project
export const Home = () => {
    const dispatch = useDispatch();

    //using the useSelector hook we finding data on the authorized user if such a user exists
    const userData = useSelector((state) => state.auth.data);
    //finding articles and tags from store with hook useSelector
    const {articles, tags} = useSelector(state => state.articles);

    //Flag of loading article
    const isArticlesLoading = articles.status === 'loading';

    //Flag of loading tags
    const isTagsLoading  = tags.status  === 'loading';

    React.useEffect(() => {
        //activate the function of request for receiving all articles
        dispatch(fetchArticles()); 
        
        //activate the function of request for receiving tags of last five articles
        dispatch(fetchTags());   
    }, [dispatch]);

    return (
        <Grid container spacing={4}>
            <Grid xs={8} item>
                { (isArticlesLoading ? [...Array(5)] : articles.items).map((obj,index) => (
                isArticlesLoading  ? (
                //articles are being uploaded
                    <Article
                        key ={index} 
                        isLoading={true}
                    />
                ) : (
                //all articles are uploaded, each one is depicted
                    <Article
                        //article ID
                        id={obj._id}           
                        //name of article                                          
                        title={obj.title}                                                     
                        //image of article
                        imageUrl={obj.imageUrl ? `http://localhost:3001${obj.imageUrl}`: ''}  
                        //user data, who wrote an article
                        user={obj.user}                                                       
                        //время создания статьи
                        createdAt={obj.createdAt}                                              
                        //the time of creating article
                        viewsCount={obj.viewsCount}   
                        //tags of article                                    
                        tags={obj.tags ? obj.tags : []}                                        
                        //the flag for editing the article, we edit the article if it was compiled by an authorized user
                        isEditable = {userData?._id === obj.user._id}
                    />
                )
                ))}
            </Grid>
            <Grid xs={4} item>
                {/*Illustration block of loaded tags */}
                <TagsBlock items={tags.items ? tags.items : []} isLoading={isTagsLoading} />
            </Grid>
        </Grid>
    );
};