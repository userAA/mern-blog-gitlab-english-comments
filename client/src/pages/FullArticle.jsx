import React from "react";
import {useParams} from "react-router-dom";
import ReactMarkdown from "react-markdown";
import axios from '../axios';

import { Article } from "../components/Article";


//the outputing page of total information about article
export const FullArticle = () => {
  //data state about article
  const [data, setData] = React.useState();
  //the flag of loading data about article
  const [isLoading, setLoading] = React.useState(true);
  //article ID
  const {id} = useParams();

  React.useEffect(() => {
    //make request on receiving full information about article with ID id
    axios.get(`/articles/${id}`).then(res => {
      setData(res.data);
      //required information is received
      setLoading(false);
    })
    .catch((err) => {
      console.warn(err);
      alert('Error when receiving the article.')
    })
  }, [id])

  //during the downloading, its template is being showing
  if (isLoading) {
    return <Article isLoading={isLoading} isFullArticle />
  }

  return (
    <>
      <Article
        //ID article
        id={data._id}
        //name article
        title={data.title}
        //image article
        imageUrl={data.imageUrl ? `http://localhost:3001${data.imageUrl}` : ''}
        //information about user, which created article
        user={data.user}
        //the time of creating article
        createdAt={data.createdAt}
        //views count of article
        viewsCount={data.viewsCount}
        //paper article
        tags={data.tags}
        //the flag of showing full information about article
        isFullArticle
      >
        <ReactMarkdown children={data.text} />
      </Article>
    </>
  );
};