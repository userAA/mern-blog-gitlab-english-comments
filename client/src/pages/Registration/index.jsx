import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import { Navigate } from "react-router-dom";
import {useForm} from 'react-hook-form'

import { Avatar, Button, Paper, TextField, Typography } from "@mui/material";

import styles from "./Registration.module.scss";

import { fetchRegister, selectIsAuth } from "../../redux/slices/auth";

export const Registration = () => {
  //using hook useSelector finding the flag of registration user 
  const isRegister = useSelector(selectIsAuth);
  const dispatch = useDispatch();

  //default data into form
  const {
    register, 
    handleSubmit, 
    formState: {errors}
  } = useForm({
    defaultValues: {
      fullname: '',
      email:    '',
      password: ''
    },
    mode: 'onChange'
  })

  const onSubmit = async (values) => {
    //activating the user registration request
    const data = await dispatch(fetchRegister(values));

    //registration user did not pass
    if (!data.payload) {
      return alert('Failed to register!')
    }

    if ('token' in data.payload) {
      window.localStorage.setItem('token', data.payload.token);
    } 
  }

  //Switching to page of user authorization, if user registration passed succesfully
  if (isRegister) {
    return <Navigate to="/" />
  }

  return (
    <Paper classes={{ root: styles.root }}>
      <Typography classes={{ root: styles.title }} variant="h5">
        Account creating
      </Typography>
      {/*Avatar of user registration */}
      <div className={styles.avatar}>
        <Avatar sx={{ width: 100, height: 100 }} />
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/*The field of the registered user's full name assignment*/}
        <TextField 
          className={styles.field} 
          error={Boolean(errors.fullname?.message)}
          helperText={errors.fullname?.message}
          type="fullname"
          {...register('fullname', {required: 'Specify fullname'})}
          label="Fullname"
          fullWidth 
        />
        {/*The field of the registered user's email assignment*/}
        <TextField 
          className={styles.field} 
          error={Boolean(errors.email?.message)}
          helperText={errors.email?.message}
          type="email" 
          {...register('email', {required: 'Specify email'})}
          label="E-Mail"
          fullWidth 
        />
        {/*The field of the registered user's password assignment*/}
        <TextField
          className={styles.field}  
          error={Boolean(errors.password?.message)}
          helperText={errors.password?.message}
          type="password" 
          {...register('password', {required: 'Specify password'})}
          label="Password"
          fullWidth 
        />
        {/*User registration request activation button*/}
        <Button type="submit" size="large" variant="contained" fullWidth>
          Register
        </Button>
      </form>
    </Paper>
  );
};