import React from "react";
import { Button, Paper, TextField } from "@mui/material";
import SimpleMDE from "react-simplemde-editor";

import "easymde/dist/easymde.min.css";
import axios from '../../axios';
import styles from "./AddArticle.module.scss";
import { useSelector } from "react-redux";
import { selectIsAuth } from "../../redux/slices/auth";
import { useNavigate, Navigate, useParams, Link } from "react-router-dom";

export const AddArticle = () => {
  //ID article (if article is being created then id empty, and if it editing then ID exists)
  const {id} = useParams();
  //navigation hook
  const navigate = useNavigate();
  //user authorization flag (does it exist authorized user)
  const isAuth = useSelector(selectIsAuth);
  //the state of text article
  const [text, setText] = React.useState("");
   //the state of title article
  const [title, setTitle] = React.useState("");
  //the state of tags article
  const [tags, setTags] = React.useState("");
  //the state of image article
  const [imageUrl, setImageUrl] = React.useState("");
  const inputFileRef = React.useRef(null);

  //the flag of editting article
  const isEditing = Boolean(id);

  const handleChangeFile = async (event) => {
    try
    {
      //generating image for article
      const formData = new FormData();
      const file = event.target.files[0];
      formData.append('image', file);

      //Uploading separate image for article in separete paper on disk via the server
      const {data} = await axios.post('/upload', formData);
      //fixing image article
      setImageUrl(data.url);
    }
    catch (err)
    {
      console.warn(err);
      alert('Error when uploading a file!');
    }
  };

  //the function of removing image from article
  const onClickRemoveImage = () => {
    setImageUrl('');  
  };

  //changing the text content of article
  const onChange = React.useCallback((value) => {
    setText(value);
  }, []);

  const onSubmit = async () => {
    try
    {
      //data field for created or edited article
      const fields = {title, imageUrl, tags, text}

      const {data} = isEditing 
        ? await axios.patch(`/articles/${id}`, fields) 
        : await axios.post('/articles', fields);

      const _id = isEditing ? id : data._id;

      //navigation on page of full image on created or editing article
      navigate(`/articles/${_id}`);
    }
    catch (err)
    {
      console.warn(err);
      alert('Error when creating an article!')
    }
  }

  React.useEffect(() => {
    if (id) {
      axios
      .get(`/articles/${id}`)
      .then(({data}) => {
        //finding the name of article with ID id
        setTitle(data.title);
        //finding the text content of article with ID id 
        setText(data.text);
        //finding the name of image in article with ID id 
        setImageUrl(data.imageUrl);
        //finding the array of tags in article with ID id
        setTags(data.tags.join(','));
      }).catch(err => {
        console.warn(err);
        alert('Error when receiving the article!');
      })
    }
  }, [id])

  //template options for component SimpleMD
  const options = React.useMemo(
    () => ({
      spellChecker: false,
      maxHeight: "400px",
      autofocus: true,
      placeholder: "Enter the text...",
      status: false,
      autosave: {
        enabled: true,
        delay: 1000,
      },
    }),
    []
  );

  //Moving on home page if the authorized user does not exist
  if (!window.localStorage.getItem('token') && !isAuth) {
    return <Navigate to="/" />
  }

  return (
    <Paper style={{ padding: 30 }}>
      {/*Uploading the image for paper */}
      <Button onClick={() => inputFileRef.current.click()} variant="outlined" size="large">
        Download preview
      </Button>
      <input 
        ref={inputFileRef} 
        type="file" 
        onChange={handleChangeFile} 
        hidden 
      />
      {imageUrl && (
        <>
          {/*The button of removing image for paper */}
          <Button variant="contained" color="error" onClick={onClickRemoveImage} >
            Remove
          </Button>
          {/*Image for paper */}
          <img className={styles.image} src={`http://localhost:3001${imageUrl}`} alt="Uploaded" />
        </>
      )}
      <br />
      <br />
      {/*The field for setting the title of the article */}
      <TextField
        classes={{ root: styles.title }}
        variant="standard"
        placeholder="Article title..."
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        fullWidth
      />
      {/*Field for setting article tags */}
      <TextField
        classes={{ root: styles.tags }}
        variant="standard"
        placeholder="Tags"
        value={tags}
        onChange={(e) => setTags(e.target.value)}
        fullWidth
      />
      {/*Editor for creating the content of the article*/}
      <SimpleMDE
        className={styles.editor}
        value={text}
        onChange={onChange}
        options={options}
      />
      <div className={styles.buttons}>
         {/*In case isEditing == true editted article saving, otherwise, the created article is published */}
        <Button onClick={onSubmit} size="large" variant="contained">
          {isEditing ? 'Save' : 'Public'} 
        </Button>
        {/*Switching on central page project*/}
        <Link to="/">
          <Button size="large">Cancel</Button>
        </Link>
      </div>
    </Paper>
  );
};
