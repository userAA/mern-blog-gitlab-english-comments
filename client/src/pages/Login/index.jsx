import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import { Navigate } from "react-router-dom";
import {useForm} from 'react-hook-form'

import { Button, Paper, TextField, Typography } from "@mui/material";

import styles from "./Login.module.scss";
import { fetchAuth, selectIsAuth } from "../../redux/slices/auth";

export const Login = () => {
  //finding the flag user authorization, using the useSelector hook
  const isAuth = useSelector(selectIsAuth);
  const dispatch = useDispatch();
  const {
    register, 
    handleSubmit, 
    formState: {errors}
  } = useForm({
    //default data into form
    defaultValues: {
      email :   '',
      password: ''
    },
    mode: 'onChange'
  })

  const onSubmit = async (values) => {
    //activating the user authorization request
    const data = await dispatch(fetchAuth(values));

    //the event, when authorization did not pass
    if (!data.payload) {
      return alert('Failed to log in')
    }

    //fixing the token of the authorized user
    if ('token' in data.payload) 
    {
      window.localStorage.setItem('token', data.payload.token);
    } 
  }

  //return to home page, if authorization passed succesfully
  if (isAuth) {
    return <Navigate to="/" />
  }

  return (
    <Paper classes={{ root: styles.root }}>
      <Typography classes={{ root: styles.title }} variant="h5">
        Log in to your account
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/*The field of the authorized user's mail assignment*/}
        <TextField 
          className={styles.field} 
          label="E-Mail"
          type="email" 
          error={Boolean(errors.email?.message)}
          helperText={errors.email?.message}
          {...register('email', {required: 'Specify email'})}
          fullWidth 
        />
        {/*The field of the authorized user's password assignment*/}
        <TextField 
          className={styles.field} 
          label="Password"
          type="password" 
          error={Boolean(errors.password?.message)}
          helperText={errors.password?.message}
          {...register('password', {required: 'Specify password'})}
          fullWidth 
        />
        {/*Activation button for user authorization request */}
        <Button type="submit" size="large" variant="contained" fullWidth>
          Enter
        </Button>
      </form>
    </Paper>
  );
};
