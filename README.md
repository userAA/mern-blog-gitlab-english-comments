gitlab page https://gitlab.com/userAA/mern-blog-gitlab-english-comments.git
gitlab comment mern-blog-gitlab-english-comments

project mern-blog (simple blog on redux toolkit)
technologies used on the frontend:
    @emotion/react,
    @emotion/styled,
    @mui/icons-material,
    @mui/material,
    @reduxjs/toolkit,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    clsx,
    easymde,
    prettier,
    react,
    react-dom,
    react-hook-form,
    react-markdown,
    react-redux,
    react-router-dom,
    react-scripts,
    react-simplemde-editor,
    sass,
    web-vitals;

technologies used on the backend:
    bcrypt,
    cors,
    express,
    express-validator,
    jsonwebtoken,
    mongoose,
    multer;
Implementing registration and authorization of user. The list of papers according all users in data base
is being output. According to authorized user one can to create new paper. The papers of authorized user one can 
to add or to remove. 