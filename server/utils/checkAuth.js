import jwt from 'jsonwebtoken'

//checking user authorization
export default (req, res, next) => {
    //let's see if there is token of user authorization
    const token = (req.headers.authorization || '').replace(/Bearer\s?/,'');
    if (token)
    {
        try 
        {
            //there is token of user authorization, define ID of the authorized user req.userId 
            const decoded = jwt.verify(token, 'secret123');
            req.userId = decoded._id;
            next();
        } 
        catch (e) 
        {
            return res.status(403).json({
                message: 'No access'
            })
        }
    }
    else
    {
        return res.status(403).json({
            message: 'No access'
        })
    }
}