import mongoose from "mongoose";

//the database model of the registered user
const UserSchema = new mongoose.Schema({
    //full name of registered user
    fullname: {
        type: String,
        required: true
    },
    //email of registered user
    email: {
        type: String,
        required: true,
        unique: true
    },
    //encrypted password of registered user
    passwordHash: {
        type: String,
        required: true
    },
    //the avatar of registered user
    avatarUrl: String
}, {
    timestamps: true
})

export default mongoose.model('User', UserSchema);