import mongoose from "mongoose";

//The model of article, creating by authorized user
const ArticleSchema = new mongoose.Schema({
    //title of article
    title: {
        type: String,
        required: true
    },
    //text content of article
    text: {
        type: String,
        required: true,
        unique: true
    },
    //tags array of article
    tags: {
        type: Array,
        default: []
    },
    viewsCount: {
        type: Number,
        default: 0    
    },
    //registered user, who created this article
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    //image of article
    imageUrl: String
}, {
    timestamps: true
})

export default mongoose.model('Article', ArticleSchema);