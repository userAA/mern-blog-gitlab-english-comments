import ArticleModel from '../models/Article.js'

//the function of receiving of last tags from data base ArticleModel on last five articles
export const getLastTags = async (req, res) => {
    try
    {
        //finding the list of five articles from data base ArticleModel
        const articles = await ArticleModel.find().limit(5).exec();
        //finding all the tags from received list of articles
        const tags = articles.map((obj) => obj.tags).flat().slice(0, 5);
        //sending received tags to frontend
        res.json(tags);
    }
    catch (err)
    {
        res.status(500).json({
            message: 'Could not get the articles'
        })
    }    
}

//function of receiving all articles from data base ArticleModel
export const getAll = async (req, res) => {
    try
    {
        //finding the list articles from data base ArticleModel with full information about user, which wrote every article
        const articles = await ArticleModel.find().populate('user').exec();
        //sending the received list of all articles to frontend
        res.json(articles);
    }
    catch (err)
    {
        console.log(err);
        res.status(500).json({
            message: 'Could not get the articles'
        })
    }
}

//receiving function of one article with ID articleId
export const getOne = async (req, res) => {
    try
    {
        //Emiting ID of article articleId, by which finding the article from model ArticleModel 
        const articleId = req.params.id;
        ArticleModel.findOneAndUpdate(
            {
                _id: articleId
            },
            {
                //views count article with ID articleId increasing by one
                $inc: {viewsCount: 1} 
            },
            {
                returnDocument: 'after'
            },
            (err, doc) => {
                //error at receiving of article 
                if (err) {
                    console.log(err);
                    return res.status(500).json({
                        message: 'Could not get the article'
                    })                   
                }

                //there is no the article with ID articleId
                if (!doc)
                {
                    return res.status(404).json({
                        message: 'Article not found'
                    })
                }
                //sending the required article
                res.json(doc);
            }
        ).populate('user');
    }
    catch (err)
    {
        console.log(err);
        res.status(500).json({
            message: 'Could not get the article'
        })
    }
}

//removing function of article with ID req.params.id
export const remove = async (req, res) => {
    try
    {
        //defining ID of removing article
        const articleId = req.params.id;
        //removing corresponding article
        ArticleModel.findOneAndDelete(
            {
                _id: articleId
            },
            (err, doc) => {
                //error at removing article
                if (err) {
                    console.log(err);
                    return res.status(500).json({
                        message: 'Could not remove the article'
                    })
                }

                //the article to be deleted was not found
                if (!doc) {
                    return res.status(404).json({
                        message: 'Article not found'
                    })
                }
                //the article to be deleted has been deleted
                res.json({
                    success: true
                })
            }
        )
        
    }
    catch (err)
    {
        console.log(err);
        res.status(500).json({
            message: 'Could not remove the article'
        })
    }
}

//the function of creating article
export const create = async (req, res) => {
    try 
    {
        const doc = new ArticleModel({
            //title of article
            title: req.body.title,
            //text content of article
            text: req.body.text,
            //the image in article
            imageUrl: req.body.imageUrl,
            //tags of article
            tags: req.body.tags.split(','),
            //ID user, which made up this article
            user: req.userId
        })

        //fixing new article in data base
        const article = await doc.save();
        //sending total information about new article to frontend
        res.json(article);
    }
    catch (err)
    {
        res.status(500).json({
            message: 'Could not create the article'
        })
    }
}

//the function of editting article
export const update = async (req, res) =>
{
    try 
    {
        //defining ID of editting article
        const articleId = req.params.id;
        //editting corresponding article
        await ArticleModel.updateOne(
            {
                _id: articleId
            },
            {
                //the title of editting article
                title: req.body.title,
                //the text content of editting article
                text: req.body.text, 
                //the image of editting article  
                imageUrl: req.body.imageUrl, 
                //ID user, which editted of article        
                user: req.userId,
                //the tags of editted article
                tags: req.body.tags
            }
        );

        //editting of article successfully finished
        res.json({
            success: true
        })
    }
    catch (err)
    {
        res.status(500).json({
            message: 'Failed to update the article'
        })
    }
}