import express from 'express';
import multer from 'multer';
import cors from 'cors';

import mongoose from 'mongoose';
import { 
    //user registration data verification function
    registerValidation, 
    //user authorization data verification function
    loginValidation, 
    //the function of data verification for creating article by authorized user
    articleCreateValidation 
} from './validations.js';

import {
    handleValidationErrors, 
    //the function of existence verification of token authorized user
    checkAuth
} from './utils/index.js';
import {
    //controller which respond for operations with user data
    UserController, 
    //controller which respond for operations with articles
    ArticleController
} from './controllers/index.js';

// use mongoose to connect this app to our database on mongoDB using the DB_URL (connection string)
mongoose.connect
(
    "mongodb://127.0.0.1:27017/mern-blog",
    {
        useNewUrlParser: true,
        useUnifiedTolology: true,
        useCreateIndex: true
    }
)
.then(() => 
{
    console.log("Succesfully connected to MongoDB");
})
.catch ((error) => 
{
    console.log("Unable to connect to MongoDB Atlas!");
    console.error(error);    
})

const app = express();

//the mechanism for saving an image in an article in a separate folder
const storage = multer.diskStorage({
    destination: (_, __, cb) => {
        cb(null, 'uploads');
    },
    filename: (_, file, cb) => {
        cb(null, file.originalname)
    }
})

//guarantees a response with requests in json format
app.use(express.json());
//guarantees the transmition information between servers with defferent local addresses
app.use(cors());

//saving image of any paper in separete paper, and user which created this paper must be authorized
const upload = multer({storage});
app.use('/uploads',express.static('uploads'));
app.post('/upload', checkAuth, upload.single('image'), (req, res) => {
    res.json({
        url: `/uploads/${req.file.originalname}`
    })
});

//router user authorization
app.post('/auth/login', loginValidation, handleValidationErrors, UserController.login);
//router user registration
app.post('/auth/register', registerValidation, handleValidationErrors, UserController.register);
//router for receiving data on an authorized user, if the token of an authorized user is fixed
app.get('/auth/me', checkAuth, UserController.getMe);

//router of receiving of last tags from data base ArticleModel on last five articles
app.get('/tags',  ArticleController.getLastTags);
//router of receiving all articles from data base ArticleModel
app.get('/articles', ArticleController.getAll);

//router of receiving of one article with ID articleId
app.get('/articles/:id', ArticleController.getOne);
//router of creating articles
app.post('/articles', checkAuth, articleCreateValidation, handleValidationErrors, ArticleController.create);
//router of removing article
app.delete('/articles/:id', checkAuth, ArticleController.remove);
//router of editing article
app.patch('/articles/:id',checkAuth, articleCreateValidation, handleValidationErrors, ArticleController.update);

app.listen(3001, (err) => {
    if (err) { 
        return console.log(err);
    }
    console.log('Server OK');
})